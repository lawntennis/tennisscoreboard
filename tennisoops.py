class TennisScoreboard:
    def __init__(self):
        self.points = {'A': 0, 'B': 0}
        self.games = {'A': 0, 'B': 0}
        self.sets = {'A': 0, 'B': 0}
        self.point_map = {0: 15, 15: 30, 30: 40, 40: 50}

    def update_points(self, player):
        self.points[player] = self.point_map.get(self.points[player], 0)
        return self.points[player] == 50

    def update_games(self, player):
        opponent = 'A' if player == 'B' else 'B'
        if self.points[opponent] != 40:
            self.games[player] += 1
            self.points['A'] = self.points['B'] = 0
            if self.games[player] >= 6 and self.games[player] - self.games[opponent] >= 2:
                self.update_sets(player)
        else:
            self.points[player] = 'A' if self.points[player] == 40 else 40

    def update_sets(self, player):
        self.sets[player] += 1
        self.games['A'] = self.games['B'] = 0

    def process_match(self, match_string):
        for point in match_string:
            if self.update_points(point):
                self.update_games(point)
        return {
            'points': {k: v if v != 50 else 0 for k, v in self.points.items()},
            'games': self.games,
            'sets': self.sets
        }

match_string = "ABABABABA"
scoreboard = TennisScoreboard()
result = scoreboard.process_match(match_string)
print(result)


