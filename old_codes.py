def update_points(player,points):
    point_map = {0:15, 15:30, 30:40, 40:50}
    points[player] = point_map.get(points[player], 0)
    return points[player] == 50

def update_games(player, games, points):
    opponent = 'A' if player == 'B' else 'B'
    if points[opponent] != 40:
        games[player] += 1
        points['A'] = points['B'] = 0
    else:
        points[player] = 'A' if points[player] == 40 else 40

def tennis_scoreboard(match_string):
    points = {'A': 0, 'B': 0}
    games = {'A': 0,'B' : 0}
    for point in match_string:
        if update_points(point, points):
            update_games(point, games, points)
    return {'points': {k: v if v != 50 else 0 for k, v in points.items()},
        'games': games}
